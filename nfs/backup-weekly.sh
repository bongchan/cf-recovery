BACKUP_HOME=/var/vcap/store/nfs-backup
DATE=`date '+%Y%m%d'`


mkdir "$BACKUP_HOME"
mkdir "$BACKUP_HOME"/meta
mkdir "$BACKUP_HOME"/data

cd ${BACKUP_HOME}

echo delete old files start

rm -rf "$BACKUP_HOME"/data/*
rm -rf "$BACKUP_HOME"/meta/*

echo delete old files end

echo backup start!

tar cpfzv "$BACKUP_HOME"/data/nfs-F-"$DATE".gz --listed-incremental "$BACKUP_HOME"/meta/backuplist --exclude=nfs-backup ../../store/

echo backup end!

echo "scp start"

 scp -i  "$BACKUP_HOME"/key/openstack.key "$BACKUP_HOME"/data/nfs-F-"$DATE".gz root@10.111.140.68:/root/backup/cf-nfs/

echo "scp end"

rm "$BACKUP_HOME"/data/nfs-F-"$DATE".gz