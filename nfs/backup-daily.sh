BACKUP_HOME=/var/vcap/store/nfs-backup
DATE=`date '+%Y%m%d'`

mkdir "$BACKUP_HOME"
mkdir "$BACKUP_HOME"/meta
mkdir "$BACKUP_HOME"/data

cd ${BACKUP_HOME}

echo backup start!

tar cpfzv "$BACKUP_HOME"/data/nfs-I-"$DATE".gz --listed-incremental "$BACKUP_HOME"/meta/backuplist --exclude=nfs-backup ../../store/

echo backup end!

echo "scp start"

scp -i  "$BACKUP_HOME"/key/openstack.key "$BACKUP_HOME"/data/nfs-I-"$DATE".gz root@10.111.140.68:/root/backup/cf-nfs/
rm "$BACKUP_HOME"/data/nfs-I-"$DATE".gz

echo "scp end"