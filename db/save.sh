#!/bin/bash

STORAGE_URL="161.202.49.231"
STORAGE_ID="ubuntu"
STORAGE_PEM="${HOME}"/cf-recovery/ssh/dev-installer.pem
STORAGE_DIR="~/stg-backup"
BACKUP_DIR=${HOME}/cf-recovery/files

echo scp start!

#echo "scp -i $STORAGE_PEM $BACKUP_DIR/* $STORAGE_ID@$STORAGE_URL:$STORAGE_DIR/"

scp -i "$STORAGE_PEM" "$BACKUP_DIR"/* "$STORAGE_ID"@"$STORAGE_URL":"$STORAGE_DIR"/

echo scp end!

