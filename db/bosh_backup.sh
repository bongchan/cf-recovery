#!/bin/bash

DATE=`date '+%Y%m%d'`
BACKUP_DIR=${HOME}/cf-recovery/files

mkdir ${HOME}/cf-recovery
mkdir ${HOME}/cf-recovery/files

echo dump start!
cd /var/vcap/packages/postgres/bin
./pg_dump -U postgres bosh > "$BACKUP_DIR"/bosh_"$DATE".sql

echo bosh dump complete!

./pg_dump -U postgres postgres > "$BACKUP_DIR"/postgres_"$DATE".sql

echo postgres dump complete!
echo dump end!

cd "$BACKUP_DIR"
cd ..
./save.sh
