#!/bin/bash

DB_PASSWORD="admin"
DB_URL="10.110.29.17"
DB_PORT="5524"

DATE=`date '+%Y%m%d'`
BACKUP_DIR=${HOME}/cf-recovery/files

mkdir ${HOME}/cf-recovery
mkdir ${HOME}/cf-recovery/files

echo dump start!

sudo PGPASSWORD="$DB_PASSWORD" pg_dump -h "$DB_URL"  -p "$DB_PORT" -U ccadmin ccdb > "$BACKUP_DIR"/ccdb_"$DATE".sql

echo ccdb dump complete!

sudo PGPASSWORD="$DB_PASSWORD" pg_dump -h "$DB_URL"  -p "$DB_PORT" -U uaaadmin uaadb > "$BACKUP_DIR"/uaadb_"$DATE".sql

echo uaadb dump complete!
echo dump end!

cd "$BACKUP_DIR"
cd ..
sudo ./save.sh
