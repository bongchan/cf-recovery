CF의 CCDB, UAADB와 bosh의 DB를 백업하기 위한 스크립트(임시)

stg 대상 구성
- CF DB백업
    - installer vm의 root/cf-recovery에 위치
    - crontab 으로 매일 22시에 cf_backup.sh수행
    - 백업한 데이터는 dev.ghama의 installer(/home/ubuntu/stg-backup)에 저장
- Bosh DB 백업
    - Bosh director vm의 root/cf-recovery에 위치
    - crontab 으로 매일 22시에 bosh_backup.sh수행
    - 백업한 DB는 dev.ghama의 installer(/home/ubuntu/stg-backup)에 저장
- nfs 백업
	- api vm의 /var/vcap/store/nfs-backup에 위치
	- crontab 으로 월~토 22시에 Incremenal 백업
	- crontab 으로 일요일 22시에 Full 백업
	- 백업 데이터는 controller /root/backup에 저장